package com.tanisorn.week8;

public class Map {
    private String name;
    private int width;
    private int height;
    
    public Map(String name, int width, int height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }
    

    public String getName() {
        return name;
    }
    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }
    public void print() {
        for (int i = 0; i < getWidth(); i++) {
            for (int j = 0; j < getHeight(); j++) {
                System.out.print("-");
            }
            System.out.println();
        }
    }
}
