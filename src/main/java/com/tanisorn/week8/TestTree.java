package com.tanisorn.week8;

public class TestTree {
    public static void main(String[] args) {
        Tree tree1 = new Tree("tree1", 5, 10);
        System.out.println(tree1.getName() + " X : " + tree1.getX() + " Y : " + tree1.getY());

        Tree tree2 = new Tree("tree2", 5, 11);
        System.out.println(tree2.getName() + " X : " + tree2.getX() + " Y : " + tree2.getY());

    }
}
