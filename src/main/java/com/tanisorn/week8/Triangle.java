package com.tanisorn.week8;

public class Triangle {
    private String name;
    private double a;
    private double b;
    private double c;

    public Triangle(String name, double a, double b, double c) { 
        this.name = name;
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public String getName() {
        return name;
    }

    public double getA() {
        return a;
    }
    public double getB() {
        return b;
    }
    public double getC() {
        return c;
    }

    public double calTriArea() {
        double area = 0;
        double s = 0; 
        s = (a+b+c)/2;
        area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
        return area;
    }

    public void printCalTriArea() {
        System.out.println(name + " area " + " = " + calTriArea());
    }

    public double calTriPerimeter() {
        double perimeter = 0;
        perimeter = (a+b+c);
        return perimeter;
    }

    public void printcalTriPerimeter(){
        System.out.println(name + " perimeter " + " = " + calTriPerimeter());
    }
}
