package com.tanisorn.week8;

public class Square {
    private String name;
    private double width;
    private double height;

    public Square(String name, double widht, double height) {
        this.name = name;
        this.width = widht;
        this.height = height;
    }

    public String getName() {
        return name;

    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double calSquareArea() {
        double area = 0;
        area = width * height;
        return area;
    }

    public void printSquareArea() {
        System.out.println(name + " area " + " = " + calSquareArea());
    }

    public double calSquareperimeter() {
        double perimeter = 0;
        perimeter = width*2 + height*2;
        return perimeter;
    }

    public void printSquareperimeter() {
        System.out.println(name + " perimeter " + " = " + calSquareperimeter());
    }

}
