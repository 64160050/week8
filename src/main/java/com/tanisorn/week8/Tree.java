package com.tanisorn.week8;

public class Tree {
    private String name;
    private int x;
    private int y;

    public Tree(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public String getName() {
        return name;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}
