package com.tanisorn.week8;

public class TastShape {
    public static void main(String[] args) {
        Square square1 = new Square("Square1", 10, 5);
        System.out.println(square1.getName() + "Widght" + " = " + square1.getWidth());
        System.out.println(square1.getName() + "Height" + " = " + square1.getHeight());
        square1.calSquareArea();
        square1.printSquareArea();
        square1.calSquareperimeter();
        square1.printSquareperimeter();

        Square square2 = new Square("Square2", 5, 3);
        System.out.println(square2.getName() + "Widght" + " = " + square2.getWidth());
        System.out.println(square2.getName() + "Height" + " = " + square2.getHeight());
        square2.calSquareArea();
        square2.printSquareArea();
        square2.calSquareperimeter();
        square2.printSquareperimeter();
        
        Circle circle1 = new Circle("Circle1", 1);
        System.out.println(circle1.getName() + "Radlus" + " = " + circle1.getRadlus());
        circle1.calCircleArea();
        circle1.printcalCirecleArea();
        circle1.calCircleperimeter();
        circle1.printcalCircleperimeter();

        Circle circle2 = new Circle("Circle2", 2);
        System.out.println(circle2.getName() + "Radlus" + " = " + circle2.getRadlus());
        circle2.calCircleArea();
        circle2.printcalCirecleArea();
        circle2.calCircleperimeter();
        circle2.printcalCircleperimeter();

        Triangle triangle1 = new Triangle("Triangle1", 5, 5, 6);
        System.out.println(triangle1.getName() + " a = " + triangle1.getA() + " b = " + triangle1.getB() + " c = " + triangle1.getC());
        triangle1.calTriArea();
        triangle1.printCalTriArea();
        triangle1.calTriPerimeter();
        triangle1.printcalTriPerimeter();
    }
}