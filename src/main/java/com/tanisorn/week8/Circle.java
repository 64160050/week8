package com.tanisorn.week8;

public class Circle {
    private double radlus;
    private String name;

    public Circle(String name, double radlus) {
        this.name = name;
        this.radlus = radlus;
    }

    public String getName() {
        return name;
    }
    public double getRadlus() {
        return radlus;
    }
    public double calCircleArea() {
        double area = 0;
        area = 3.14*(radlus*radlus);
        return area;
    }

    public void printcalCirecleArea() {
        System.out.println(name + " area " + " = " + calCircleArea());
    }

    public double calCircleperimeter() {
        double perimeter = 0;
        perimeter = Math.PI*radlus*2;
        return perimeter;
    }

    public void printcalCircleperimeter() {
        System.out.println(name + " perimter " + " = " + calCircleperimeter());
    }
}
